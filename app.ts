import { app, BrowserWindow, ipcMain } from 'electron';
import { enableLiveReload } from 'electron-compile';
const robot = require("robotjs");
const Jimp = require("jimp");
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow: Electron.BrowserWindow | null;

const isDevMode = process.execPath.match(/[\\/]electron/);

if (isDevMode) enableLiveReload();

const createWindow = async () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
  });

  // and load the index.html of the app.
  mainWindow.loadURL(`file://${__dirname}/index.html`);

  // Open the DevTools.
  if (isDevMode) {
    mainWindow.webContents.openDevTools();
  }

  // Emitted when the window is closed.
  mainWindow.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.

ipcMain.on('takeSnapshot', (event, status) => {
  var imgBuffer = robot.screen.capture();
  screenCaptureToBase64(imgBuffer, event);
})

//quit app from angular
ipcMain.on('quit', (event, status) => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
  event.returnValue =  {error : null, success : "done"};
})

var screenCaptureToBase64 = function (robotScreenPic, event) {
  try {
      const image = new Jimp(robotScreenPic.width, robotScreenPic.height);
      let pos = 0;
      image.scan(0, 0, image.bitmap.width, image.bitmap.height, (x, y, idx) => {
      /* eslint-disable no-plusplus */
      image.bitmap.data[idx + 2] = robotScreenPic.image.readUInt8(pos++);
      image.bitmap.data[idx + 1] = robotScreenPic.image.readUInt8(pos++);
      image.bitmap.data[idx + 0] = robotScreenPic.image.readUInt8(pos++);
      image.bitmap.data[idx + 3] = robotScreenPic.image.readUInt8(pos++);
      /* eslint-enable no-plusplus */
      });

      image.getBase64( Jimp.MIME_PNG, (error, base64data) => {
        if(error) event.returnValue = {error : error, res : null};
        else
           event.returnValue =  {error : null, imgdata : base64data};
      });
  } catch (e) {
      event.returnValue = {error : e, res : null};
  }
}
